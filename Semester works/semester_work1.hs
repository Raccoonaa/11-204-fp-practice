module Main where

data Expr = Literal String 
                | Lambda String Expr
                | Apply Expr Expr
                deriving (Eq, Show)

--3 types of expression for b-reduction

varEx, abstrEx, applyEx :: Expr
varEx = Literal "var"
abstrEx = Lambda "var" varEx
applyEx = Apply abstrEx varEx

exprFunc :: Expr -> k
exprFunc exp = case exp of 
                    Literal str -> undefined str
                    Lambda str1 exp3 -> undefined str1 (exprFunc exp3)
                    Apply exp1 exp2 -> undefined (exprFunc exp1) (exprFunc exp2)

type Context = [(String, Expr)]

--Dictionary entry for b-reduction

contextFunc :: Context -> k
contextFunc cont = undefined (head cont) contextFunc (tail cont)

eval :: Expr -> Expr
eval t = eval'[] t

eval' :: Context -> Expr -> Expr
eval' context exp = case exp of
                l@(Literal k)   -> maybe l id (lookup k context)
                (Lambda s t)    -> Lambda s (eval' context t)
                (Apply t1 t2)   -> apply context (eval' context t1) (eval' context t2)

apply :: Context -> Expr -> Expr -> Expr
apply context t1 t2 = case t1 of
                    (Lambda s t)    ->  eval' ((s, t2):context) t
                    otherwise       ->  Apply t1 t2
