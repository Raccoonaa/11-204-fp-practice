module Main where

-- | Нахождение максимум и его индекс 
-- >>> myMaxIndex [1,5,2,3,4]
-- (1,5)

myMaxIndex :: [Integer] -> (Int,Integer)
myMaxIndex [] = (-1, 0)
myMaxIndex (x:xs) = exec (0, x) 1 xs
    where exec k i [] = k
          exec k i (x:xs) = if x > (snd k)
                          then exec (i, x) (i + 1) xs
                          else exec k (i + 1) xs
						  
-- | Количество элементов, равных максимальному
-- >>> maxCount [1,5,3,10,3,10,5]
-- 2

maxCount :: [Integer] -> Int
maxCount [] = 0
maxCount (x:xs) = exec2 (1, x) xs
    where exec2 k [] = (fst k)
          exec2 k (x:xs) = if x > (snd k)
                          then exec2 (1, x) xs
                          else if x == (snd k)
                                then exec2 ((fst k) + 1, x) xs
                                else exec2 k xs

-- | Количество элементов между минимальным и максимальным
-- >>> countBetween [-1,3,100,3]
-- 2
--
-- >>> countBetween [100,3,-1,3]
-- -2
--
-- >>> countBetween [-1,100]
-- 1
--
-- >>> countBetween [1]
-- 0


myMinIndex :: [Integer] -> (Int, Integer)
myMinIndex [] = (-1, 0)
myMinIndex (x:xs) = myMinIndex' (0, x) 1 xs
	where 
		myMinIndex' mx ind [] = mx
		myMinIndex' mx ind (x:xs) = if x < (snd mx)
								   then myMinIndex' (ind, x) (ind + 1) xs
								   else myMinIndex' mx (ind + 1) xs

countBetween :: [Integer] -> Int
countBetween [] = 0
countBetween [x] = 0
countBetween l = (fst (myMaxIndex l)) - (fst (myMinIndex l))
