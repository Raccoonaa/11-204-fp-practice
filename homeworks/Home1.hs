module Main where

{- 
 -
 -                4
 - pi = -------------------
 -                1^2
 -       1 + --------------
 -                   3^2
 -            2 + ---------
 -                     5^2
 -                 2 + ----
 -                      ...
 -}
 
pi1 n | n > 0 = 4 / (1 + (temp n 1))
    where temp 1 k = k * k
          temp n k = (k * k) / (2 + (temp (n - 1) (k + 2)))
		  
{-
 -                  1^2
 - pi = 3 + -------------------
 -                   3^2
 -           6 + -------------
 -                     5^2
 -                6 + ------
 -                     ...
 -}
 
pi2 n | n > 0 = 3 + (1 / (temp2 n 3))
    where temp2 1 k = 6
          temp2 n k = (6 + (k * k) / temp2 (n - 1) (k + 2))

{-
 -                 4
 - pi = ------------------------
 -                   1^2
 -       1 + ------------------
 -                    2^2
 -            3 + -----------
 -                     3^2
 -                 5 + ---
 -                     ...
 -}
 
pi3 n | n > 0 = 4 / (1 + (temp3 n 1))
    where temp3 1 k = k ^ 2
          temp3 n k = (k ^ 2) / (1 + 2 * k + (temp3 (n - 1) (k + 1)))
 
{-       4     4     4     4
 - pi = --- - --- + --- - --- + ...
 -       1     3     5     7
 -}
 
pi4 n | n > 0 = 4 + minus n 1
    where minus 1 k = 0
          minus n k = -4 / (k * 2 + 1) + plus (n - 1) (k + 1)
          plus 1 k = 0
          plus n k = 4 / (k * 2 + 1) + minus (n - 1) (k + 1)

{-             4         4         4         4
 - pi = 3 + ------- - ------- + ------- - -------- + ...
 -           2*3*4     4*5*6     6*7*8     8*9*10
 -}
 
pi5 n | n > 0 = 3 + plus n 2
    where plus 1 k = 0
          plus n k = 4 / (product[k..k+2]) + minus (n - 1) (k + 2)
          minus 1 k = 0
          minus n k = -4 / (product[k..k+2]) + plus (n - 1) (k + 2)

{-
       x^1     x^2
e^x = ----- + ----- + ... + 1 
        1!      2!
-}

e x n | n > 0 = val x n 1 1 1
    where val x 0 k m p = 1
          val x n k m p = (x * p) / m + val x (n - 1) (k + 1) (m * (k + 1)) (p * x)
